export default () =>(
    <nav>

        <a href=""><img src="/logo.svg" alt=""/></a>
        <ul>
            <li> <i className="ion-android-notifications"></i> </li>
            <li>Status Online <i className="ion-record"></i></li>
            <li><span><i className="ion-android-person"></i></span></li>
            <li><i className="ion-android-more-vertical"></i></li>
        </ul>

        <style jsx>{`
            nav{
                height: 64px;
                // background-color: pink;
                padding: 0 2rem;
                display: flex;
                justify-content: space-between;
                position: fixed;
                width: 100%;
                background-color: white;
                z-index: 999;
            }
            a{
                height: 100%;
                display: flex;
                align-items: center;
            }
            img{
                height: 30px;
            }
            ul{
                width: 20rem;
                display: flex;
                justify-content: space-between;
                // border: 1px solid red;
            }
            ul li{
                display: inline;
                list-style: none;
                // font-size: 1.2rem;
            }
            i{
                color: grey;
            }
            .ion-record{
                color: #33ff33;
            }
            span{
                display: inline-block;
                width: 1.5rem; 
                height: 1.5rem;
                display: flex;
                justify-content: center;
                align-items: center;
                // padding: .1rem;
                border: 2px solid purple;
                border-radius: 50%;

            }
            span > i{
                color: purple;
            }
            .ion-android-more-vertical{
                font-size: 1.2rem;
            }
            
        `}</style>
    </nav>
)