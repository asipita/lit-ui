import Table from '../components/Table'
export default () =>(
    <section>
        <div className="tableContainer">
            <div className="prices">
                <div id="platinum">
                    Plan features
                </div>
                <div className="bronze">
                    <div className="subscription">BRONZE</div>
                    <div className="price">
                        <div>
                            500 
                        </div>
                        <small>Per Month</small>
                    </div>
                    <button>Activate</button>
                </div>
                <div className="blueGuy">
                    <div className="subscription">SILVER</div>
                    <div className="price">
                        <div>
                            2000 
                        </div>
                        <small>Per Month</small>
                    </div>
                    <button id="whiteButton">Activate</button>
                </div>
                <div className="gold">
                    <div className="subscription">GOLD</div>
                    <div className="price">
                        <div>
                            2500 
                        </div>
                        <small>Per Month</small>
                    </div>
                    <button>Activate</button>
                </div>
            </div>
        <Table />
        </div>
        <div className="footer">
            <div>
                <img src="/logo-white.svg" alt=""/>
            </div>
            <div className="info">
                <span>Terms & conditions</span>
                <span>Career</span>
                <span>Privacy Policy</span>
                <span>Leapsail Copyright 2020</span>
            </div>
            <div className="info">
                <i className="ion-social-instagram"></i>
                <i className="ion-social-twitter"></i>
                <i className="ion-social-facebook"></i>
                <i className="ion-social-linkedin"></i>
            </div>
            <div></div>
        </div>
        <style jsx>
            {`
                section{
                    background-color: #F5F5F5;
                }
                .tableContainer{
                    position: relative;
                    bottom: 15vh;
                    background-color: white;
                    width: 85%;
                    border-radius: 65px;
                    margin: 0 auto;
                    box-shadow: 5px 5px 20px rgba(0,0,0,0.2);
                    min-height: 40rem;
                    padding: 2rem;
                }
                .prices{
                    display: grid;
                    grid-template-columns: 1fr 1fr 1fr 1fr;
                }
                .prices > div{
                    position: relative;
                    bottom: 3rem;
                    height: 22rem;
                    // border: 2px solid grey;
                    padding: 3rem 1rem 2rem;
                    text-align center;
                    display: flex;
                    flex-direction: column;
                    justify-content: space-between;
                }
                #platinum{
                    font-size: 2.5rem;
                    font-weight: 800;
                    justify-content: flex-end;
                    text-align: left;
                    color: #656565;
                }
                .subscription{
                    font-size: 2rem;
                    font-weight: bold;
                }
                .price > div{
                    font-size: 3rem;
                    font-weight: bold;
                    color: #656565;
                }
                .price small{
                    font-size: 1rem;
                    font-weight: normal;
                    color: #D2D2D2;
                }
                .prices > div > button{
                    height: 51px;
                    border: none;
                    background: #FF6700;
                    box-shadow: 0px 3px 10px #00000029;
                    border-radius: 46px;
                    color: white;
                    font-size: 1.0rem;
                }
                .blueGuy{
                    background-color: #0369ffed;
                    height: 120%;
                    color: white;
                    border-radius: 20px;
                }
                #whiteButton{
                    background-color: white;
                    color: #FF6700;
                }
                .blueGuy .price div{
                    color: white;
                }
                .blueGuy .price{
                    color: white;
                }

                .bronze .subscription{
                    color: #00D154;
                    font-weight: 900;
                }

                .gold .subscription{
                    color: #7A00D1;
                    font-weight: 900;
                }
                .footer{
                    height: 64px;
                    background-color: #242424;
                    padding: 0 2rem;
                    display: flex;
                    justify-content: space-between;
                    align-items: center;
                }
                .footer img{
                    height: 40px;
                }
                .footer span, .footer i{
                    padding: 0 1.5rem;
                    color: whitesmoke;
                    font-size:0.7rem;
                }
                .footer span:last-child{
                    border: none;
                }
                .footer i{
                    font-size:1rem; 
                }
            `}
        </style>
    </section>
)