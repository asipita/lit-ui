export default () =>(
    <section>
        <div className="descriptionBox">
            Our pricing <br/>  plan for everyone.
            <div>Packages are for individual only</div>
        </div>

        <style jsx>{`
            section{
                height: 75vh;
                padding-top: 64px;
                
                // background: green;
            }
            .descriptionBox{
                // width: 250px;
                // border: 1px solid grey;
                font-size: 2.5rem;
                font-weight: 900;
                // position: relative;
                padding-top: 5rem;
                padding-left: 131px;
                width: 500px;
                // height: 153px;
                text-align: left;
                font-family: 'Segoe UI';
                letter-spacing: 0px;
                color: #242424;
                opacity: 1;
            }

            .descriptionBox > div{
                margin: .5rem 0;
                font-size: 1.5rem;
                color: #D2D2D2;
                font-weight: normal;
            }
        `}</style>
    </section>
)