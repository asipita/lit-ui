export default () => (
    <section>
        <div>
            <div id="title">Loan</div>
            <div className="green-icon">50k - 150k Naira</div>
            <div className="blue-icon">50k - 2M Naira</div>
            <div className="purple-icon">50k - 40M Naira</div>
        </div>
        <div>
            <div id="title">Investment</div>
            <div><i className="green-icon ion-ios-checkmark-outline"></i></div>
            <div><i className="blue-icon ion-ios-checkmark-outline"></i></div>
            <div><i className="purple-icon ion-ios-checkmark-outline"></i></div>
        </div>
        <div>
            <div id="title">Smart Travel Savings</div>
            <div><i className="green-icon ion-ios-checkmark-outline"></i></div>
            <div><i className="blue-icon ion-ios-checkmark-outline"></i></div>
            <div><i className="purple-icon ion-ios-checkmark-outline"></i></div>
        </div>
        <div>
            <div id="title">Security</div>
            <div><i className="green-icon ion-ios-checkmark-outline"></i></div>
            <div><i className="blue-icon ion-ios-checkmark-outline"></i></div>
            <div><i className="purple-icon ion-ios-checkmark-outline"></i></div>
        </div>
        {/* 
        -
        */}
        <div>
            <div id="title">Bank Statement</div>
            <div className="green-icon">200k - 400k Naira</div>
            <div className="blue-icon">200k - 2M Naira</div>
            <div className="purple-icon">200k - 40M Naira</div>
        </div>
        <div>
            <div id="title">Visa</div>
            <div><i className="green-icon ion-ios-checkmark-outline"></i></div>
            <div><i className="blue-icon ion-ios-checkmark-outline"></i></div>
            <div><i className="purple-icon ion-ios-checkmark-outline"></i></div>
        </div>
        <div>
            <div id="title">Ticket</div>
            <div><i className="green-icon ion-ios-checkmark-outline"></i></div>
            <div><i className="blue-icon ion-ios-checkmark-outline"></i></div>
            <div><i className="purple-icon ion-ios-checkmark-outline"></i></div>
        </div>
        <div>
            <div id="title">Escort</div>
            <div></div>
            <div><i className="blue-icon ion-ios-checkmark-outline"></i></div>
            <div><i className="purple-icon ion-ios-checkmark-outline"></i></div>
        </div>
        {/* 
        -
        */}
        <div>
            <div id="title">Eight Free Country Destination</div>
            <div className="green-icon">Just 1</div>
            <div className="blue-icon">Just 3</div>
            <div><i className="purple-icon ion-ios-checkmark-outline"></i></div>
        </div>
        <div>
            <div id="title">Tour package</div>
            <div><i className="green-icon ion-ios-checkmark-outline"></i></div>
            <div><i className="blue-icon ion-ios-checkmark-outline"></i></div>
            <div><i className="purple-icon ion-ios-checkmark-outline"></i></div>
        </div>
        <div>
            <div id="title">Hotel Reservation</div>
            <div><i className="green-icon ion-ios-checkmark-outline"></i></div>
            <div><i className="blue-icon ion-ios-checkmark-outline"></i></div>
            <div><i className="purple-icon ion-ios-checkmark-outline"></i></div>
        </div>
        <div>
            <div id="title">24/7 Customer Success Expert</div>
            <div></div>
            <div></div>
            <div><i className="purple-icon ion-ios-checkmark-outline"></i></div>
        </div>
        <div>
            <div id="title">Travel Shows</div>
            <div><i className="green-icon ion-ios-checkmark-outline"></i></div>
            <div><i className="blue-icon ion-ios-checkmark-outline"></i></div>
            <div><i className="purple-icon ion-ios-checkmark-outline"></i></div>
        </div>
        <div>
            <div id="title">Event</div>
            <div></div>
            <div><i className="blue-icon ion-ios-checkmark-outline"></i></div>
            <div><i className="purple-icon ion-ios-checkmark-outline"></i></div>
        </div>
        <div>
            <div id="title">Games</div>
            <div></div>
            <div></div>
            <div><i className="purple-icon ion-ios-checkmark-outline"></i></div>
        </div>
         {/* 
        -
        */}
        <div>
            <div id="title">Gold Investor</div>
            <div className="green-icon"></div>
            <div className="blue-icon">Silent Investor</div>
            <div><i className="purple-icon ion-ios-checkmark-outline"></i></div>
        </div>
        <div>
            <div id="title">Start Up</div>
            <div></div>
            <div><i className="blue-icon ion-ios-checkmark-outline"></i></div>
            <div><i className="purple-icon ion-ios-checkmark-outline"></i></div>
        </div>
        <div>
            <div id="title">Give a Meal</div>
            <div><i className="green-icon ion-ios-checkmark-outline"></i></div>
            <div><i className="blue-icon ion-ios-checkmark-outline"></i></div>
            <div><i className="purple-icon ion-ios-checkmark-outline"></i></div>
        </div>
        <div>
            <div id="title">Cargo</div>
            <div><i className="green-icon ion-ios-checkmark-outline"></i></div>
            <div><i className="blue-icon ion-ios-checkmark-outline"></i></div>
            <div><i className="purple-icon ion-ios-checkmark-outline"></i></div>
        </div>


        <style jsx>
            {`
                section > div{
                    display: grid;
                    grid-template-columns: 1fr 1fr 1fr 1fr;
                    // background: green;
                }
                section > div:nth-child(odd){
                    background: #FBFBFB;
                    border-radius: 20px;
                }
                section > div > div{
                    height: 4rem;
                    display: flex;
                    justify-content: center;
                    align-items: center;
                    // border: 1px solid red;
                }
                #title{
                    justify-content: flex-start;
                    padding-left: 1rem;
                    color: #656565;
                    font-weight: 600;
                }
                i{
                    font-size: 2rem;
                    font-weight: bold;
                }
                .green-icon{
                    color: #00D154;
                    font-weight: 600;
                }
                .blue-icon{
                    color: #0090E8;
                    font-weight: 600;
                }
                .purple-icon{
                    color: #7A00D1;
                    font-weight: 600;
                }
            `}
        </style>
    </section>
)